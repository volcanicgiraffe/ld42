/**
 *  For single player, so both wasd and cursors will work
 */
export class AnyInput {
  constructor() {
    this.kb1 = new WASDInput();
    this.kb2 = new CursorsInput();
  }

  holdUp() {
    return this.kb1.holdUp() || this.kb2.holdUp() || game.pad1.justDownUp() || game.pad2.justDownUp();
  }

  holdDown() {
    return this.kb1.holdDown() || this.kb2.holdDown() || game.pad1.justDownDown() || game.pad2.justDownDown();
  }

  holdLeft() {
    return this.kb1.holdLeft() || this.kb2.holdLeft() || game.pad1.justDownLeft() || game.pad2.justDownLeft();
  }

  holdRight() {
    return this.kb1.holdRight() || this.kb2.holdRight() || game.pad1.justDownRight() || game.pad2.justDownRight();
  }

  pressedKick() {
    return this.kb1.pressedKick() || this.kb2.pressedKick() || game.pad1.justDownAny() || game.pad2.justDownAny();
  }
}

export class CursorsInput {
  constructor() {
    this.cursors = game.input.keyboard.createCursorKeys();
    this.kick = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
  }

  holdUp() {
    return this.cursors.up.isDown || game.pad2.justDownUp();
  }

  holdDown() {
    return this.cursors.down.isDown || game.pad2.justDownDown();
  }

  holdLeft() {
    return this.cursors.left.isDown || game.pad2.justDownLeft();
  }

  holdRight() {
    return this.cursors.right.isDown || game.pad2.justDownRight();
  }

  pressedKick() {
    return this.kick.justPressed() || game.pad2.justDownAny();
  }
}

export class WASDInput {
  constructor() {
    this.up = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.down = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.left = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.right = game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.kick = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  }

  holdUp() {
    return this.up.isDown || game.pad1.justDownUp();
  }

  holdDown() {
    return this.down.isDown || game.pad1.justDownDown();
  }

  holdLeft() {
    return this.left.isDown || game.pad1.justDownLeft();
  }

  holdRight() {
    return this.right.isDown || game.pad1.justDownRight();
  }

  pressedKick() {
    return this.kick.justPressed() || game.pad1.justDownAny();
  }
}
