export const TILE_SIZE = 120;
export const Y_FACTOR = 0.5;

export const ICE_COUNT_LOSE = 9;
export const PROGRESS_WIN = 12; // boat wood;

export const PLAYER_SPEED_ICE = 400;
export const PLAYER_SPEED_FLOAT = 150;
export const PLAYER_KF_FLOAT = 400;
export const PLAYER_KF = 800;

export const REWARD_PENG_DEATH = 10;
export const REWARD_WOOD_BOAT = 50;
export const REWARD_KICK_ANY = 1; // reward for kick. affected by combo
