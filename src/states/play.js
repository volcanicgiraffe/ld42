import IslandLevel from "../model/IslandLevel";
import { SinglePlayer, TwoPlayers } from "../model/GameMode";


export class WaterShader extends Phaser.Filter {
   constructor() {
     super(game);
     this.fragmentSrc = `
        precision mediump float;
        
        varying vec2       vTextureCoord;
        uniform float      time;
        uniform sampler2D  uSampler;
        uniform vec2       resolution;

        #define WAVES 10.
        #define WSCALEY 6.
        #define TIMESCALEY 2.
        #define TIME_SPEED_X -0.01
        #define TIME_SPEED_INC_PER_ROW 0.1
        #define WOFFSET -0.5
        #define WAVE_COUNT 60.
        #define EDGE 0.2

        float rand(vec2 co){
            return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
        }

        void main(void) {
            float onWave = 0.0;
            float onGlare = 0.0;
            for (int i = 0; i < int(WAVES); i++) {
               float scaleTime = TIMESCALEY*((cos(time + vTextureCoord.x*10.) + 1.)/0.5 + 1.);
               float yInWave = WAVES * (vTextureCoord.y - (float(i)*1.0 / WAVES)) * (WSCALEY + scaleTime);
               float speedX = mod(time * (TIME_SPEED_X), 3.1415) + float(i)*TIME_SPEED_INC_PER_ROW;
                                            
               onWave = max(onWave, step(abs(sin((vTextureCoord.x + speedX + WOFFSET*float(i))*WAVE_COUNT) - yInWave), EDGE));

               float inWave = vTextureCoord.y > yInWave && vTextureCoord.y < yInWave + 1.0/WAVES ? 1.0 : 0.0;
               float glare = step(rand(vec2(floor(gl_FragCoord.x/8.+ speedX), vTextureCoord.y+gl_FragCoord.y)), 0.001);
               glare = glare * (0.5 + 0.5*step((1. + sin(time + vTextureCoord.x + rand(gl_FragCoord.xy)))*0.5, 0.5));
               onGlare = max(onGlare, glare);
            }
            gl_FragColor = texture2D(uSampler, vTextureCoord);
            gl_FragColor.rgb *= (0.8 + 0.2*onWave);
            gl_FragColor = clamp(gl_FragColor*(1.0 + onGlare), 0., 1.);
            //gl_FragColor.r = onGlare;
        }
   `;
   }

}

export function createWater() {
    let bgBitmap = game.add.bitmapData(1, 1);
    bgBitmap.fill(0x62, 0x90, 0xc3);
    let bgSprite = game.add.sprite(0, 0, bgBitmap);
    bgSprite.width = game.world.width;
    bgSprite.height = game.world.height;
    bgSprite.filters = [new WaterShader()];
    bgSprite.update = () => bgSprite.filters[0].update();
    return bgSprite;
}

export default {
  init({ mode }) {
    this.mode = mode;
  },

  create() {
    game.add.tween(game.world).to({ alpha: 1 }, 100, null, true);

    game.stage.backgroundColor = "#6290c3";
    this.bgSprite = createWater();
    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.p2.defaultRestitution = 0.8;
    game.level = new IslandLevel(this.mode);
    game.level.init();



    // this._theme = game.add.sound('arctic_1');
    // this._theme.play(undefined, undefined, 0, true);
    // this._theme.volume = 0.7;
  },


  update() {
    game.pad1.update();
    game.pad2.update();

    game.level.update();
  },


  render() {
    game.debug.text('FPS: ' + (game.time.fps || '--'), 4, game.canvas.height - 24);
    // game.debug.text('Player 1 state: ' + ((game.level && game.level.players && game.level.players[0] && game.level.players[0].state.constructor.name) || '--'), 4, game.canvas.height - 64);
    // game.debug.text('Player 2 state: ' + ((game.level && game.level.players && game.level.players[1] && game.level.players[1].state.constructor.name) || '--'), 4, game.canvas.height - 44);
    //
    // game.debug.text('Penguins buttbooted: ' + game.level.penguinCounter, game.canvas.width - 300, game.canvas.height - 24);
    // game.debug.text('TIME: ' + Math.round(game.level.gameTimer / 1000), game.canvas.width - 300, game.canvas.height - 44);
  },

  shutdown() {
    // this._theme.destroy();
    game.time.removeAll();
    game.level.destroyLevel();
  }
};


