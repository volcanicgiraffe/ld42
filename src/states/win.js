
module.exports = {
  init() {
    game.world.alpha = 1;
    game.world.setBounds(0, 0, 1280, 720);
    game.stage.backgroundColor = '#27282D';

  },

  create() {
    let imgName;
    if (!game.gameState.winned) {
      imgName = "lost";
    } else if (game.gameState.eventsResults.lastChance && !game.gameState.eventsResults.lastChance.pants) {
      imgName = "win_pl"
    } else {
      imgName = "win";
    }
    let sp = game.add.sprite(0, 0, imgName);
    sp.alpha = 0;
    game.add.tween(sp).to({ alpha: 1 }, 1000, null, true);

    let historyLines = []; //["something", "something else", "something something something something something something something something something something something "];
    let usedIds = new Set();
    for (let { id, msg } of game.gameState.history) {
      if (!usedIds.has(id)) {
        usedIds.add(id);
        historyLines.push(msg);
      }
    }
    let bmh = game.add.bitmapText(300, 260, "nokia", 'In his travels, he:', 18);
    let bm = game.add.bitmapText(300, 300, "nokia", historyLines.map(h => " - " + h).join("\n"), 18);
    bm.maxWidth = 600;
    bmh.tint = 0xc2893c;
    bm.tint = 0xc2893c;
    sp.addChild(bm);
    sp.addChild(bmh);



    game.input.keyboard.enabled = true;
    sp.inputEnabled = true;
    sp.input.useHandCursor = true;
    let next = () => {
      game.input.keyboard.onDownCallback = undefined;
      game.add.tween(game.world)
        .to({ alpha: 0 }, 100, null, true)
        .onComplete
        .add(() => game.state.start("play"));
    };
    sp.events.onInputUp.addOnce(next);
    //game.input.keyboard.onDownCallback = next;

    if (typeof ga === "function") ga('send', 'event', 'game', 'end');
  }
};