import { SinglePlayer, TwoPlayers } from "../model/GameMode";
import { createWater } from "./play";

export default {
  create() {
    game.add.tween(game.world).to({ alpha: 1 }, 100, null, true);

    game.stage.backgroundColor = "#6290c3";
    this.bgSprite = game.add.sprite(0, 0, 'splash');
    this.bgSprite.width = game.world.width;
    this.bgSprite.height = game.world.height;

    // this.title = game.add.text(game.world.width / 2, 200, 'POLYARNIK', { font: "bold 150px Arial" });
    // this.title.anchor.set(0.5);
    // this.title.rotation = -Math.PI / 100;
    // game.add.tween(this.title).to({ rotation: Math.PI / 100 }, 1500, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);

    this.tooltip = game.add.text(game.world.width - 90, 220, '', { font: "24px Arial" });
    this.tooltip.fill = '#FFFFFF';
    this.tooltip.anchor.set(1, 0);

    this.cursors = game.input.keyboard.createCursorKeys();
    this.keys = {
      space: game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
      enter: game.input.keyboard.addKey(Phaser.Keyboard.ENTER),
    };

    this._initMenu();

    this._theme = game.add.sound('arctic_ambient');
    this._theme.play(undefined, undefined, 0, true);
    this._theme.volume = 0.5;

    if (typeof ga === "function") ga('send', 'event', 'game', 'menu');
  },

  _initMenu() {
    this.selectedItem = 0;

    this.items = [
      {
        text: "1 PLAYER",
        action: () => {
          game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
            game.state.start("play", undefined, undefined, { mode: SinglePlayer });
          });
        },
        tooltip: 'Controls: wasd/arrows - move, space/enter - kick'
      },
      {
        text: '2 PLAYERS',
        action: () => {
          game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
            game.state.start("play", undefined, undefined, { mode: TwoPlayers });
          });
        },
        tooltip: 'P1: wasd - move, space - kick\nP2: arrows - move, enter - kick'
      }
    ];

    this.labels = [];

    for (let i = 0; i < this.items.length; i++) {
      let item = this.items[i];
      let label = game.add.text(game.world.width - 90, 350 + i * 42, item.text, { font: "bold 40px Arial" });
      label.anchor.set(1, 0);
      label.fill = '#FFFFFF';
      this.labels.push(label);
    }

    this.pointer = game.add.text(game.world.width - 370, 150, '>', { font: "bold 40px Arial" });
    this.pointer.fill = '#FFFFFF';
    game.add.tween(this.pointer).to({ x: this.pointer.x - 3 }, 300, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
  },

  update() {
    game.pad1.update();
    game.pad2.update();

    if (this.cursors.up.justDown || game.pad1.justDownUp(true) || game.pad2.justDownUp(true)) {
      this.selectedItem = (this.items.length + this.selectedItem - 1) % this.items.length;

    } else if (this.cursors.down.justDown || game.pad1.justDownDown(true) || game.pad2.justDownDown(true)) {
      this.selectedItem = (this.selectedItem + 1) % this.items.length;

    }
    this.pointer.y = 350 + this.selectedItem * 42;
    this.tooltip.text = this.items[this.selectedItem].tooltip;

    if (this.keys.enter.justDown || this.keys.space.justDown || game.pad1.justDownAny() || game.pad2.justDownAny()) {
      this.items[this.selectedItem].action();
      game.sounder.play('ice');
    }
  },

  shutdown() {
    for (let label of this.labels) label.destroy();
    // this.title.destroy();
    this.pointer.destroy();
    // this._theme.destroy();
  }
};
