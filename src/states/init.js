export default {
  init() {
    // itch.io iframe focus fix
    game.input.mspointer.capture = false;
  },

  preload() {
    game.time.advancedTiming = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL ;
    game.scale.setResizeCallback(function() {
      game.scale.setMaximum();
    });

    game.load.image('loading', 'assets/loading.png');
    game.load.image('loading_giraffe', 'assets/giraffe.png');
    game.load.image('loading_island', 'assets/island.png');
  },

  create() {
    game.state.start('load');
  }
};
