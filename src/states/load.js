import Sounder from "../utility/Sounder";
import PadManager from "../utility/PadManager";

export default {
  loadingLabel() {
    let loading = game.add.sprite(game.world.centerX, game.world.centerY + 100, "loading");
    loading.anchor.setTo(0.5, 0.5);

    let bar = game.add.sprite(game.world.centerX, game.world.centerY - 45, "loading_giraffe");
    bar.anchor.setTo(0.5, 1);

    let barBg = game.add.sprite(game.world.centerX, game.world.centerY, "loading_island");
    barBg.anchor.setTo(0.5, 0.5);

    game.load.setPreloadSprite(bar, 1);
  },

  preload() {
    this.loadingLabel();
    game.load.atlas('tiles', 'assets/tiles.png', 'assets/tiles.json');
    game.load.atlas('actors', 'assets/actors.png', 'assets/actors.json');
    game.load.atlas('bp', 'assets/bp.png', 'assets/bp.json');
    game.load.atlas('effects', 'assets/effects.png', 'assets/effects.json');

    game.load.image('arrow', 'assets/arrow.png'); // todo: atlas, animation
    game.load.image('splash', 'assets/splash.png');

    this._loadMusic();
    this._loadSounds();
  },

  _loadMusic() {
    game.load.audio('arctic_1', 'assets/audio/Alexey Grishin - arctic-1.ogg');
    game.load.audio('arctic_ambient', 'assets/audio/arctic_ambient.ogg');
  },

  _loadSounds() {
    game.load.audio('splash_1', 'assets/audio/splash1.wav');
    game.load.audio('splash_2', 'assets/audio/splash2.wav');
    game.load.audio('splash_3', 'assets/audio/splash3.wav');

    game.load.audio('snow_1', 'assets/audio/snow_0_1.wav');
    game.load.audio('snow_2', 'assets/audio/snow_0_2.wav');
    game.load.audio('snow_3', 'assets/audio/snow_0_3.wav');

    game.load.audio('ice_1', 'assets/audio/ice_0_1.wav');
    game.load.audio('ice_2', 'assets/audio/ice_0_2.wav');
    game.load.audio('ice_3', 'assets/audio/ice_0_3.wav');
    game.load.audio('ice_4', 'assets/audio/ice_0_4.wav');

    game.load.audio('hello_1', 'assets/audio/hello1.wav');

    game.load.audio('cry_1', 'assets/audio/cry1.wav');
    game.load.audio('cry_2', 'assets/audio/cry2.wav');
    game.load.audio('cry_3', 'assets/audio/cry3.wav');
    game.load.audio('cry_4', 'assets/audio/cry4.wav');
    game.load.audio('cry_5', 'assets/audio/cry5.wav');
    game.load.audio('cry_6', 'assets/audio/cry6.wav');

    game.load.audio('hit_1', 'assets/audio/hit1.wav');
    game.load.audio('hit_2', 'assets/audio/hit2.wav');
    game.load.audio('hit_3', 'assets/audio/hit3.wav');
    game.load.audio('hit_4', 'assets/audio/hit4.wav');
  },

  create() {
    game.sounder = new Sounder();

    game.input.gamepad.start();
    game.pad1 = new PadManager(game.input.gamepad.pad1);
    game.pad2 = new PadManager(game.input.gamepad.pad2);

    game.state.start("menu");
  }
};
