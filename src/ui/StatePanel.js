import { PROGRESS_WIN, REWARD_WOOD_BOAT } from "../consts";

export default class StatePanel extends Phaser.Group {

  constructor(x, y) {
    super(game, x, y);
    game.ui.add(this);

    this.p1Score = game.add.text(50, 50, '');
    prettify(this.p1Score, 28, '#DD4466', '#FF99FF', 4);
    this.p2Score = game.add.text(game.world.width - 400, 50, '');
    prettify(this.p2Score, 28, '#3651ae', '#9999FF', 4);

    this.addChild(this.p1Score);
    this.addChild(this.p2Score);

    this.woodProgress = game.add.text(game.world.width / 2, 74, 'COLLECT BOXES INTO THE VESSEL');
    this.woodProgress.anchor.set(0.5);
    prettify(this.woodProgress, 48, '#333333', '#FFFF99', 4);
    game.add.tween(this.woodProgress.scale).to({
      x: 1.1,
      y: 1.1
    }, 300, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);

    game.time.events.add(6000, () => {
      game.add.tween(this.woodProgress).to({ alpha: 0, }, 400, Phaser.Easing.Cubic.In, true, 0).onComplete.addOnce(() => {
        game.tweens.removeFrom(this.woodProgress.scale);
        this.showScore = true;
        this.woodProgress.text = "0 / 10";
        this.woodProgress.scale.set(1.0);

        game.add.tween(this.woodProgress).to({ alpha: 1, }, 400, Phaser.Easing.Cubic.In, true, 0);
      });
    });

    this.addChild(this.woodProgress);


    this.status = game.add.text(game.world.width / 2, game.world.height / 2 - 250, '');
    prettify(this.status, 64, '#333333', '#FFFF99', 4);
    this.status.alpha = 0;
    this.status.anchor.set(0.5);
    this.addChild(this.status);

    this.pressAny = game.add.text(game.world.width / 2, game.world.height / 2, '');
    prettify(this.pressAny, 32, '#333333', '#FFFF99', 4);
    this.pressAny.alpha = 0;
    this.pressAny.anchor.set(0.5);
    this.pressAny.text = 'PRESS ANY KEY TO RESTART';
    this.addChild(this.pressAny);
  }

  update() {

    if (game.level.players[1]) {
      this.p1Score.text = `P1 SCORE: ${game.level.players[0].score}`;
      this.p2Score.text = `P2 SCORE: ${game.level.players[1].score}`;
    } else {
      this.p1Score.text = `SCORE: ${game.level.players[0].score}`;
    }

    if (this.showScore) {
      this.woodProgress.text = `${game.level.boat.progress} / ${PROGRESS_WIN}`;
    }
  }

  showResults(status = 'win') {

    game.add.tween(this.p1Score).to({
      x: game.world.width / 2,
      y: game.world.height / 2 - 150,
      fontSize: 40
    }, 1500, Phaser.Easing.Quadratic.In, true, 0);

    game.add.tween(this.p2Score).to({
      x: game.world.width / 2,
      y: game.world.height / 2 - 100,
      fontSize: 40
    }, 1500, Phaser.Easing.Quadratic.In, true, 0);

    game.add.tween(this.p1Score.anchor).to({ x: 0.5, }, 1500, Phaser.Easing.Cubic.In, true, 0);
    game.add.tween(this.p2Score.anchor).to({ x: 0.5, }, 1500, Phaser.Easing.Cubic.In, true, 0);

    this.status.text = status === 'win' ? 'YOU ARE SAFE NOW :)' : 'PENGUINS GOT YOU :('; // I'm such a genius at wording >_<
    game.add.tween(this.status).to({ alpha: 1.0, }, 1000, Phaser.Easing.Cubic.In, true, 0);

    this.pressAny.rotation = -Math.PI / 64;
    game.add.tween(this.pressAny).to({ rotation: Math.PI / 64 }, 1200, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);

    game.time.events.add(1000, () => {
      game.add.tween(this.pressAny).to({ alpha: 1.0, }, 1500, Phaser.Easing.Cubic.In, true, 0);
    })
  }
}

function prettify(label, size = 12, fill = '#FFFFFF', strokeFill = '#000000', stroke = Math.round(size / 2)) {
  label.font = 'Arial';
  label.fontSize = size;
  label.fill = fill;
  label.stroke = strokeFill;
  label.strokeThickness = stroke;
  label.setShadow(2, 2, "#333333", 2, true, false);
}
