export default class LogicGroup {
  constructor(parentRealGroup) {
    this.children = [];
    this.parent = parentRealGroup;
  }

  add(sprite) {
    this.children.push(sprite);
    this.parent.add(sprite);
    let oldDestroy = sprite.destroy;
    sprite.destroy = (a, b, c) => {
      let idx = this.children.indexOf(sprite);
      if (idx !== -1) {
        this.children.splice(idx, 1);
      }
      oldDestroy.call(sprite, a, b, c);
    }

  }

  forEach(cb) {
    this.children.forEach(cb);
  }

  forEachAlive(cb) {
    for (let c of this.children) {
      if (c.alive) {
        cb(c);
      }
    }
  }

  getClosestTo(object, cb, ctx) {
    return Phaser.Group.prototype.getClosestTo.call(this, object, cb, ctx);
  }

  get total() {
    return this.children.length;
  }

  remove(sprite, ...args) {
    let idx = this.children.indexOf(sprite);
    if (idx !== -1) {
      this.children.splice(idx, 1);
    }
    this.parent.remove(sprite, ...args);
  }

  destroy() {
    //ignore
  }
}