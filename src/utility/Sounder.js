export default class Sounder {

  constructor() {
    this.groups = new Map();

    this.add('ice', ['ice_1', 'ice_2', 'ice_3', 'ice_4'], 100);
    
    this.add('hit', ['hit_1', 'hit_2', 'hit_3', 'hit_4'], 300);
    this.add('splash', ['splash_1', 'splash_2', 'splash_3'], 300);

    this.add('snow', ['snow_1', 'snow_2', 'snow_3'], 420);
    this.add('hello', ['hello_1'], 420);
    this.add('cry', ['cry_1', 'cry_2', 'cry_3','cry_4','cry_5','cry_6'], 420);

    // this.add('single_sound');
  }

  destroy() {
    for (let [name, group] of this.groups.entries()) {
      group.sounds.forEach(s => s.destroy());
    }
  }

  add(group, sounds = [group], cooldown = 500) {
    this.groups.set(group, {
      sounds: sounds.map(name => this.addSound(name, group)),
      cooldown: cooldown,
      canPlay: true
    });
  }

  addSound(name, group) {
    let s = game.add.sound(name);
    s.allowMultiple = true;
    s.onPlay.add(() => {
      this.groups.get(group).canPlay = false;
      game.time.events.add(this.groups.get(group).cooldown, () => this.groups.get(group).canPlay = true);
    });
    return s;
  }

  play(group) {
    if (this.groups.get(group).canPlay) {
      game.rnd.pick(this.groups.get(group).sounds).play();
    }
  }

  resetAll() {
    for (let [name, group] of this.groups.entries()) {
      group.canPlay = true;
    }
  }
}
