import Grid from "./grid";
import { generateShiftIsland } from "./islandGenerators";
import Penguin from "objects/penguin";
import { ICE_COUNT_LOSE, PROGRESS_WIN, TILE_SIZE, Y_FACTOR } from "../consts";
import Overlay from "../objects/Overlay";
import Boat from "../objects/Boat";
import Wood from "../objects/Wood";
import StatePanel from "../ui/StatePanel";
import LogicGroup from "../utility/LogicGroup"

const GRID_W = 10;
const GRID_H = 10;

export default class IslandLevel {

  constructor(gameMode) {
    this.OFFSET_X = Math.round((game.world.width - GRID_W * TILE_SIZE) / 2) + TILE_SIZE / 2; // wtf TILE_SIZE / 2 ?
    this.OFFSET_Y = Math.round((game.world.height - GRID_H * TILE_SIZE * Y_FACTOR) / 2);

    this.gameMode = gameMode;
    this.allGrp = game.add.group(undefined, "All");
    this.islandGrp = new LogicGroup(this.allGrp, "Island");
    this.trailsGrp = game.add.group(undefined, "Trails");
    this.shadowsGrp = new LogicGroup(this.allGrp, "Shadows");
    this.actorsGrp = new LogicGroup(this.allGrp, "Actors");
    this.effectsGrp = game.add.group(undefined, "Effects");
    this.grid = new Grid(GRID_W, GRID_H, this);
    this.penguinSTMax = 10000;
    this.penguinSpawnTimer = 0;

    this.woodSpawnTimer = 5000;
    this.waterLevel = 0;

    this.gameTimer = 0;
    this.penguinCounter = 0;
    generateShiftIsland(this.grid, 6, 7);

    this.initUI();
  }

  init() {
    this.boat = new Boat(game.world.width / 2, this.OFFSET_Y + (GRID_H) * TILE_SIZE * Y_FACTOR + TILE_SIZE * Y_FACTOR * 2);
    this.actorsGrp.add(this.boat);

    this.gameMode.init(this);
  }

  initUI() {
    game.ui = game.add.group(game.world, 'UI');
    game.ui.fixedToCamera = true;

    this.overlay = new Overlay();
    game.ui.addChild(this.overlay);

    this.statePanel = new StatePanel();
  }

  update() {
    this._sortUnits();

    this.checkWinCondition();
    if (this.completed) return;
    this.gameTimer += game.time.physicsElapsedMS;

    this.woodSpawn();
    this.penguinSpawn();
    if (this.grid.lvMax) {
      this.waterLevel = this.grid.iceCount / this.grid.lvMax;
    }

  }

  _sortUnits() {
    this.allGrp.sort('sortY')
  }

  woodSpawn() {
    this.woodSpawnTimer -= game.time.physicsElapsedMS;
    if (this.woodSpawnTimer > 0) return;

    let spawnZones = [
      new Phaser.Rectangle(400, 200, game.world.width - 400 * 2, game.world.height - 300 * 2),
    ];

    this.woodSpawnTimer = Math.random() * this.gameMode.woodRate;
    let point = game.rnd.pick(spawnZones).random();

    let box = new Wood(point.x, point.y, this.grid);
    box.appear();
    this.actorsGrp.add(box);
  }

  penguinSpawn() {
    this.penguinSpawnTimer -= game.time.physicsElapsedMS;
    if (this.penguinSpawnTimer > 0) return;

    let spawnZones = [
      new Phaser.Rectangle(0, 0, game.world.width, 200),
      new Phaser.Rectangle(0, 0, 200, game.world.height),
      new Phaser.Rectangle(game.world.width - 200, 0, 200, game.world.height),
    ];

    let point = game.rnd.pick(spawnZones).random();

    this.penguinSpawnTimer = this.penguinSTMax;
    this.penguinSTMax *= 0.9;
    this.penguinSTMax = Math.max(this.penguinSTMax, 1200);
    this.actorsGrp.add(new Penguin(point.x, point.y, this.grid));
  }

  checkWinCondition() {
    if (this.completed) return;

    if (this.boat.progress >= PROGRESS_WIN) {
      this.finitaLaComedia('win');
      return true;
    }

    if (this.grid.iceCount <= ICE_COUNT_LOSE) {
      this.finitaLaComedia('lose');
      return true;
    }
  }

  scoreAllPlayers(val) {
    this.players.forEach(p => { p.score += val; });
  }

  finitaLaComedia(status) {
    this.completed = true;
    this.overlay.doShow();
    for (let actor of this.actorsGrp.children) if (actor.body) actor.body.setZeroVelocity();

    game.time.events.add(2000, () => {
      game.input.keyboard.onDownCallback = () => {
        game.input.keyboard.onDownCallback = undefined;
        game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
          game.state.start("menu");
        });
      };
    });
    this.statePanel.showResults(status);
  }

  destroyLevel() {
    this.allGrp.destroy(true);
    this.islandGrp.destroy(true);
    this.trailsGrp.destroy(true);
    this.shadowsGrp.destroy(true);
    this.actorsGrp.destroy(true);
    this.effectsGrp.destroy(true);
  }

  get gridH() {
    return GRID_H;
  }

  get gridW() {
    return GRID_W;
  }
}
