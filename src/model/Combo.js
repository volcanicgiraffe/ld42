import { REWARD_KICK_ANY } from "../consts";

const COMBO_TIMEOUT = 2000;

export default class Combo {
  constructor(player) {
    this.value = 0;
    this.timer = 0;
    this.player = player;
  }

  update() {
    if (this.timer < 0) {
      if (this.value > 0) {
        /// TODO: report value?
        // console.log("### COMBO: ", this.value);

        this.value = 0;
      }
    } else {
      this.timer -= game.time.physicsElapsedMS;
    }
  }

  increment(count = 1) {
    if (count < 1) return;
    this.value += count;
    this.timer = COMBO_TIMEOUT;

    this.player.score += REWARD_KICK_ANY * this.value;
  }
}
