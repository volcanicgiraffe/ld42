import IceTile from "./IceTile";
import { TILE_SIZE, Y_FACTOR } from "consts";

export default class Grid {
  constructor(w, h, level) {
    this.level = level;
    this.grp = level.islandGrp;
    this.data = [];
    this.width = w;
    this.height = h;

    for (let i = 0; i < w; i++) {
      let col = [];
      for (let j = 0; j < h; j++) {
        col.push({ type: 'empty' });
      }
      this.data.push(col);
    }

    this.iceCount = 0;
  }

  addIce(x, y) {
    this.data[x][y] = new IceTile(this.level.OFFSET_X + TILE_SIZE * x, this.level.OFFSET_Y + TILE_SIZE * y * Y_FACTOR);
    this.data[x][y].gridX = x;
    this.data[x][y].gridY = y;
    this.grp.add(this.data[x][y]);

    this.iceCount += 1;
  }

  recalcWaterLevel() {
    this.lvMax = this.iceCount;
  }

  isIce(gx, gy) {
    if (this.isOutside(gx, gy)) return false;
    return this.data[gx][gy].type === 'ice'
  }

  isOutside(gx, gy) {
    return gx < 0 || gx >= this.width || gy < 0 || gy >= this.height;
  }

  getPointAtWorld(x, y) {
    // TODO
  }

  removeTile(x, y) {
    this.grp.remove(this.data[x][y]);
    this.data[x][y].destroy();
    this.data[x][y] = { type: 'empty' };

    this.iceCount -= 1;
    this.grp.children.forEach((t) => t.onWaterLevelChange(this.iceCount / this.lvMax));
  }

  getAt(x, y) {
    return (x, y);
  }

  forEachIceTile(cb) {
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        if (this.data[x][y].type !== 'empty') {
          cb(this.data[x][y]);
        }
      }
    }
  }

  pickClosestTile(x, y) {
    if (this.iceCount === 0) { return; }
    // tupoi perebor. can be replaced by breadth-first
    let max = 999999999;
    let result = null;
    this.grp.children.forEach((t) => {
      let d = Phaser.Point.distance(t, { x, y });
      if (d < max) {
        max = d;
        result = t;
      }
    });
    return result;
  }

  pickRndIceTile() {
    return game.rnd.pick(this.grp.children);
  }
}

