const OVERFLOW_MAX = 90;
const OVERFLOW_MIN = 30;
export default class IceTile extends Phaser.Sprite {

  constructor(x, y) {
    super(game, x, y, 'tiles', 'tile_ice1.png');
    this.health = 100;
    this.anchor.set(0.5, 0.5);
    this.overflow = new Overflow(0, 0);
    this.addChild(this.overflow);
    this.overflow.setBase(OVERFLOW_MAX);
  }

  damage(v) {
    super.damage(v);
    if (this.health === 100) {
      this.frameName = 'tile_ice1.png';
    } else {
      this.frameName = `tile_ice${Math.round((1 - (this.health / 100)) * 7) + 1}.png`;
    }
  }

  get type() {
    return 'ice';
  }

  destroy() {
    this.overflow = null;
    super.destroy(true);
    let emitter = game.add.emitter(this.x, this.y, 15);
    emitter.makeParticles('effects', [0, 1, 2, 3], 40, false, false);
    emitter.gravity = 800;
    emitter.width = 60;
    emitter.setAlpha(1, 0, 1600, Phaser.Easing.Quadratic.Out);
    emitter.minParticleSpeed.set(-200, -250);
    emitter.maxParticleSpeed.set(200, -400);
    emitter.start(true, 800, false, 15);
    game.time.events.add(600, () => emitter.destroy());
  }

  onWaterLevelChange(v) {
    this.overflow.setBase((OVERFLOW_MAX - OVERFLOW_MIN) * v + OVERFLOW_MIN);
  }

  get sortY() {
    return this.y;
  }

}

class Overflow extends Phaser.Sprite {
  constructor(x, y) {
    super(game, x, y, 'tiles', 'water_overflow.png');
    this.anchor.set(0.5, 1);
    this.baseY = y;
  }
  setBase(v) {
    this.baseY = v;
    this.y = v;
    game.tweens.removeFrom(this);
    let tween = game.add.tween(this).to({ y: this.baseY + 5 }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, -1);
    tween.yoyo(true, 0);
  }
}
