
// TODO more interesting generators

export function generateSquareIsland(grid, size) {
  const data = grid.data;
  if (data.length < size || data[0].length < size) throw new Error('Grid too small')
  const offsetU = Math.ceil((data.length - size) / 2);
  const offsetD = Math.floor((data.length - size) / 2);
  const offsetL = Math.ceil((data[0].length - size) / 2);
  const offsetR = Math.floor((data[0].length - size) / 2);
  for (let i = offsetU; i <= data.length - offsetD - 1; i++) {
    for (let j = offsetL; j <= data[0].length - offsetR - 1; j++) {
      grid.addIce(i, j);
    }
  }
  grid.recalcWaterLevel();

}

export function generateShiftIsland(grid, mWidth, height) {
  const data = grid.data;
  if (data.length < mWidth + 3 || data[0].length < height) throw new Error('Grid too small')
  const offsetU = Math.ceil((data.length - height) / 2);
  const offsetD = Math.floor((data.length - height) / 2);
  const offsetL = Math.ceil((data[0].length - mWidth) / 2);
  const offsetR = Math.floor((data[0].length - mWidth) / 2);
  for (let i = offsetU; i <= data.length - offsetD - 1; i++) {
    let rowStart = offsetL + game.rnd.integerInRange(-4, Math.abs(i - data.length * 0.5));
    let rowEnd = data.length - 1 - offsetR + game.rnd.integerInRange(-Math.abs(i - data.length * 0.5), 4);
    for (let j = Math.max(0, rowStart); j <= Math.min(rowEnd, data[0].length - 1); j++) {
      grid.addIce(i, j);
    }
  }
  grid.recalcWaterLevel();

}

