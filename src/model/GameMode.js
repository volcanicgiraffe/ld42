import { AnyInput, CursorsInput, WASDInput } from "../controllers/Keyboard";
import { TILE_SIZE, Y_FACTOR } from "../consts";
import Player from "../objects/player";

export const SinglePlayer = {
  woodRate: 10000,

  init: function (level) {
    let p1 = new Player(game.world.width / 2, level.OFFSET_Y + (level.gridH / 2) * TILE_SIZE * Y_FACTOR);
    p1.assignController(new AnyInput());
    level.actorsGrp.add(p1);

    level.players = [p1];
  }
};

export const TwoPlayers = {
  woodRate: 20000,

  init: function (level) {
    let p1 = new Player(game.world.width / 2 - 200, level.OFFSET_Y + (level.gridH / 2) * TILE_SIZE * Y_FACTOR);
    p1.assignController(new WASDInput());
    level.actorsGrp.add(p1);


    let p2 = new Player(game.world.width / 2 + 200, level.OFFSET_Y + (level.gridH / 2) * TILE_SIZE * Y_FACTOR, '2');
    p2.assignController(new CursorsInput());
    level.actorsGrp.add(p2);

    level.players = [p1, p2];
  }
};
