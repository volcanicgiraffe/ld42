import { PROGRESS_WIN } from "../consts";

const COLOR_SCHEME = {
  0: {
    fill: '#fae17f',
    stroke: "#deab62"
  },
  1: {
    fill: '#ffabe4',
    stroke: "#fdf88b"
  },
  2: {
    fill: '#689bff',
    stroke: "#fdf88b"
  }
};

const HYPE = function (label, hype, colorScheme = 1) {
  label.font = 'Arial Black';
  label.fontSize = 28;
  label.fontWeight = 'bold';
  label.fill = COLOR_SCHEME[colorScheme].fill;
  label.stroke = COLOR_SCHEME[colorScheme].stroke;
  label.strokeThickness = 12;
  label.setShadow(2, 2, "#333333", 2, true, false);
  label.tweenDelay = 500;

  if (hype > 2) {
    label.fontSize = 32;
  }
  if (hype > 3) {
    label.fontSize = 36;
  }
  if (hype > 4) {
    label.fontSize = 42;
    label.tweenDelay = 600;
    label.strokeThickness = 14;
  }
  if (hype > 5) {
    label.tweenDelay = 800;
    label.strokeThickness = 16;
  }

  return label;
};


export default class MagicText {
  constructor(x, y, text = 'CCCombo!', hype = 1, colorScheme = 1) {
    let label = game.make.text(x, y, text, { font: "bold 42px Arial" });

    label.anchor.set(0.5);
    label.scale.set(0.3);
    label.alpha = 0.3;

    label = HYPE(label, hype, colorScheme);

    game.level.effectsGrp.add(label);

    let fy = y - 80;
    let t1 = game.add.tween(label.scale).to({ x: 1, y: 1 }, 100, Phaser.Easing.Quadratic.In, true);
    let t15 = game.add.tween(label).to({ alpha: 1 }, 100, Phaser.Easing.Quadratic.In, true);
    let t25 = game.add.tween(label).to({ alpha: 0, y: fy }, label.tweenDelay, Phaser.Easing.Quadratic.In, false, 150);
    //let t2 = game.add.tween(label.scale).to({ x: 0, y: 0 }, label.tweenDelay, Phaser.Easing.Quadratic.In, false, 50);
    t1.onComplete.addOnce(() => {
      t25.start();
      //t2.start();
    });
    t25.onComplete.addOnce(() => {
      label.destroy();
    });
  }
}
