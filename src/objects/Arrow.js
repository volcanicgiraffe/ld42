export default class Arrow extends Phaser.Sprite {
  constructor() {
    super(game, 0, 0, 'arrow');

    this.anchor.set(0.5);
    this.pivot.x = -30;

    game.add.tween(this.scale).to({ x: 1.2, y: 1.2 }, 300, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
  }
}
