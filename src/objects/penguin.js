import Stateful, { DefaultState, Shadow, Splash } from './StatefulP2Sprite';
import { REWARD_PENG_DEATH, TILE_SIZE } from 'consts';
import MagicText from "../effects/MagicText";
import { Y_FACTOR } from "../consts";

export default class Penguin extends Stateful {
  constructor(x, y, grid) {
    super(x, y, 'actors', 'penguin_i.png');
    this.anchor.set(0.5, 1);
    this.body.mass = 12;
    this.body.clearShapes();
    this.body.addCircle(28, 0, -36);
    // this.body.collideWorldBounds = false;
    // this.body.updateCollisionMask();
    this.animations.add('idle', ['penguin_i.png'], 18, true);
    this.animations.add('walk', Phaser.Animation.generateFrameNames('penguin_w', 1, 5, '.png'), 24, true);
    this.animations.add('bulk', Phaser.Animation.generateFrameNames('splash', 1, 10, '.png'), 18);
    this.animations.add('stunned', ['penguin_s.png'], 18, true);
    this.animations.add('tumbling', ['penguin_t1.png', 'penguin_t2.png', 'penguin_t3.png', 'penguin_t4.png'], 22, true);
    this.animations.add('dolboebingPre', ['penguin_d1.png', 'penguin_d2.png'], 18);
    this.animations.add('dolboebing1', ['penguin_d3.png', 'penguin_d4.png', 'penguin_d5.png'], 28, true);
    this.animations.add('dolboebing2', ['penguin_d2.png', 'penguin_d3.png', 'penguin_d4.png', 'penguin_d5.png', 'penguin_d1.png'], 18, true);
    this.animations.add('underwater', ['penguin_f.png'], 18, true);
    this.animations.add('jump', Phaser.Animation.generateFrameNames('penguin_j', 1, 4, '.png'), 18);
    this.animations.add('land', Phaser.Animation.generateFrameNames('penguin_j', 4, 8, '.png'), 18);
    this.grid = grid;
    this.shadow = new Shadow(0, 0, this, this.parent);
    this.shadowOffset = 8;
    if (!this.grid.isIce(this.gx, this.gy)) {
      this.setState(new FloatIdleState(this));
    } else {
      this.setState(new IdleState(this));
    }

    this.trail = [];
    for (let i = 0; i < 3; i++) {
      let tr = new PenguinTrail(this);
      game.level.trailsGrp.addChild(tr);
      this.trail.push(tr);
    }

    this.trailEnabled = false;
    this.justKicked = false;
  }

  get gy() {
    return Math.round((this.y + (this._inWater ? (game.level.waterLevel * -40) : 0) - game.level.OFFSET_Y) / TILE_SIZE / Y_FACTOR);
    // badly looks in below
    //return Math.round((this.y + (this._inWater ? (game.level.waterLevel * -40) : 20) - game.level.OFFSET_Y) / TILE_SIZE / Y_FACTOR);
  }

  applyKick(f) {
    this.state.applyKick(f);
  }

  setState(state) {
    this.state = state;
  }

  get type() {
    return 'penguin';
  }

  update() {
    super.update();
    if (this.trailEnabled) this.updateTrail();
  }

  updateTrail() {

    let head = this.trail[this.trail.length - 1];
    head.oldX = this.x;
    head.oldY = this.y;
    head.x = this.x;
    head.y = this.y;
    head.alpha = 0.1;
    head.frameName = this.frameName;
    head.anchor.set(this.anchor.x, this.anchor.y);
    head.scale.set(this.scale.x, this.scale.y);
    head.crop(this.cropRect);

    for (let i = 1; i < this.trail.length; i++) {
      let prev = this.trail[i - 1]; // 0, 1, 2..
      let current = this.trail[i]; // 1, 2, 3..

      prev.oldX = prev.x;
      prev.oldY = prev.y;

      prev.x = current.oldX;
      prev.y = current.oldY;

      prev.anchor.set(current.anchor.x, current.anchor.y);
      prev.scale.set(current.scale.x, current.scale.y);
      prev.frameName = current.frameName;
      prev.crop(current.cropRect);

      prev.alpha = i / this.trail.length;
    }

  }

  destroy() {
    for (let tr of this.trail) tr.destroy();
    super.destroy();
  }


  resetTrail() {
    this.trailEnabled = false;
    for (let tr of this.trail) {
      tr.alpha = 0;
      tr.y = -100;
    }
  }
}

class PenguinState extends DefaultState {
  applyKick(force) {
    this.p.justKicked = true;
    game.sounder.play('cry');
    game.tweens.removeFrom(this.p);
    game.tweens.removeFrom(this.p.anchor);
    this.p.body.data.type = 1;
    this.p.body.velocity.x = force.x;
    this.p.body.velocity.y = force.y;
    if (this instanceof FlyingState) {
      new MagicText(this.p.x, this.p.y - this.p.height, `FOOTBALL!`, 2, this.p.combo.player.id);
      if (this.p.body.sprite.lastPlayerTouched) this.p.body.sprite.lastPlayerTouched.score += 30;
    }
    if (this instanceof JumpState) {
      new MagicText(this.p.x, this.p.y - this.p.height, game.rnd.pick(['Denied!', 'Get off!', 'NOPE']), 1, this.p.combo.player.id);
      if (this.p.body.sprite.lastPlayerTouched) this.p.body.sprite.lastPlayerTouched.score += 20;
    }
    this.switchTo(FlyingState);
  }

  onHit(body) {
    game.tweens.removeFrom(this.p);
    game.tweens.removeFrom(this.p.anchor);
    if (body && body.sprite && body.sprite.combo && body.sprite.type !== 'player') {
      this.p.attachCombo(body.sprite.combo);
      if (this.p.combo.value > 1) new MagicText(this.p.x, this.p.y - this.p.height, `x ${this.p.combo.value}`, this.p.combo.value, this.p.combo.player.id);
      if (this instanceof JumpState) {
        new MagicText(this.p.x, this.p.y - this.p.height, 'AIR DENIAL!', 3, this.p.combo.player.id);
        if (body.sprite.lastPlayerTouched) body.sprite.lastPlayerTouched.score += 50;
      }
    }

    if (body && body.sprite && body.sprite.isFlying) {
      if (body.sprite.type === 'player') {
        if (this instanceof JumpState) {
          new MagicText(this.p.x, this.p.y - this.p.height, 'POLYARNIK DENIAL!', 5, body.sprite.combo && body.sprite.combo.player.id);
          game.level.scoreAllPlayers(150);
        } else {
          new MagicText(this.p.x, this.p.y - this.p.height, 'POLYARNIK CANNON!', 3, body.sprite.combo && body.sprite.combo.player.id);
          game.level.scoreAllPlayers(50);
        }
      } else if (body.sprite.type === 'penguin') {
        if (this.p.isFlying && this.p.justKicked && body.sprite.justKicked && this.p.lastPlayerTouched !== body.sprite.lastPlayerTouched) {
          new MagicText(this.p.x, this.p.y - this.p.height, 'LARGE PENGUIN COLLIDER', 5, body.sprite.combo && body.sprite.combo.player.id);
          game.level.scoreAllPlayers(150);
        }
      }
      game.sounder.play('cry');
      this.p.body.data.type = 1;
      const force = Phaser.Point.subtract(this.p, body.sprite).normalize().multiply(400, 400);
      this.p.body.velocity.x = force.x;
      this.p.body.velocity.y = force.y;
      this.p.justKicked = false;
      this.switchTo(FlyingState);
    }

  }
}

class IdleState extends PenguinState {
  enter() {
    this.p.justKicked = false;
    game.tweens.removeFrom(this.p);
    this.p.inWater = false;
    this.p.shadow.visible = true;
    this.moveState = MoveState;
    this.p.isFlying = false;
    this.p.body.setZeroVelocity();
    this.p.body.data.type = 4;
    this.p.animations.play('idle');
    this.doomatTimer = game.rnd.integerInRange(300, 1000);
  }

  handleInput() {
    // AI
    if (!this.p.target) {
      this.p.target = this.p.grid.isIce(this.p.gx, this.p.gy)
        ? this.p.grid.pickRndIceTile()
        : this.p.grid.pickClosestTile(this.p.x, this.p.y);
      if (this.p.target) {
        this.p.target.tint = 0xFFEEEE;
      }
    }
  }

  update() {
    this.p.body.setZeroVelocity();
    this.doomatTimer -= game.time.physicsElapsedMS;
    if (this.doomatTimer < 0) {
      this.switchTo(this.moveState);
    }
  }
}

class DyingState extends PenguinState {
  enter() {
    this.p.inWater = true;
    this.p.shadow.visible = false;
    this.p.isFlying = false;
    this.p.body.setZeroVelocity();
    this.p.body.clearShapes();
    this.p.animations.play('bulk');
    game.sounder.play('splash');
    this.doomTimer = 600;
  }

  handleInput() {
  }

  applyKick(force) {
  }

  onHit(body) {
  }

  update() {
    this.doomTimer -= game.time.physicsElapsedMS;
    if (this.doomTimer < 0 && this.p.alive) {
      this.p.kill();

      if (this.p.lastPlayerTouched) {
        this.p.lastPlayerTouched.score += REWARD_PENG_DEATH;
      }
    }
  }
}

class SplashState extends PenguinState {
  enter() {
    this.p.inWater = true;
    this.p.shadow.visible = false;
    this.p.isFlying = false;
    this.p.body.setZeroVelocity();
    this.p.body.clearShapes();
    this.p.animations.play('bulk');
    game.sounder.play('splash');
    this.timer = 600;
  }

  handleInput() {
  }

  applyKick(force) {
  }

  onHit(body) {
  }

  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer < 0) {
      this.switchTo(FloatIdleState)
    }
  }
}

class MoveState extends PenguinState {
  enter() {
    game.tweens.removeFrom(this.p);
    this.p.inWater = false;
    this.p.shadow.visible = true;
    this.p.animations.play('walk');
    this.speed = 100;
  }

  handleInput() {
    // AI
    if (!this.p.target || this.p.target.alive === false) {
      this.p.target = null;
      this.switchTo(IdleState);
      return;
    }
    if (Phaser.Point.distance(this.p.target, this.p) < 1) {
      this.switchTo(DolboState);
    }
  }


  update() {
    this.p.body.setZeroVelocity();
    const target = Phaser.Point.subtract(this.p.target, this.p).normalize().multiply(this.speed, this.speed);
    this.p.scale.set(-Math.sign(target.x) || this.p.scale.x, 1);
    this.p.body.velocity.x = target.x;
    this.p.body.velocity.y = target.y;
  }
}

class DolboState extends PenguinState {
  enter() {
    this.p.animations.play('dolboebingPre').onComplete.addOnce(() => {
      this.p.animations.play('dolboebing' + game.rnd.pick(['1', '2']));
    });
    this.dmgTime = 5000;
    this.p.body.setZeroVelocity();
  }

  handleInput() {
    // AI
    if (!this.p.target || this.p.target.alive === false) {
      this.p.target = null;
      this.switchTo(SplashState);
      return;
    }
  }

  update() {
    if (this.p.frameName === 'penguin_d4.png') game.sounder.play('ice');
    // if (Math.round(game.time.time / 90) % 2 === 0) this.p.anchor.y = 1; else this.p.anchor.y = 0.95; // TODO tmp anim

    this.p.target.damage(game.time.physicsElapsedMS / this.dmgTime * 100);

    if (this.p.target.health <= 0) {
      this.p.grid.removeTile(this.p.target.gridX, this.p.target.gridY);
    }
  }
}


class LandOnZhopaState extends PenguinState {
  enter() {
    this.p.justKicked = false;
    this.p.body.damping = 0.4;
    game.add.tween(this.p.anchor)
      .to({ y: 1 }, 300, Phaser.Easing.Bounce.Out)
      .start();
    this.timer = 300;

  }

  handleInput() {
  }

  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (!this.p.grid.isIce(this.p.gx, this.p.gy)) {
      this.switchTo(DyingState);
      return;
    }
    if (this.timer <= 0) {
      this.switchTo(IdleState);
      this.p.resetTrail();
    }
  }
}

class FloatIdleState extends IdleState {
  enter() {
    this.p.inWater = true;
    this.p.shadow.visible = false;
    this.p.animations.play('underwater');
    this.p.body.clearShapes();
    this.p.body.setZeroVelocity();
    this.p.body.data.type = 4;
    this.moveState = FloatState;
    this.doomatTimer = game.rnd.integerInRange(300, 1000);
  }
}


class FloatState extends MoveState {
  enter() {
    this.p.inWater = true;
    this.p.shadow.visible = false;
    this.speed = 120;
    this.p.body.setZeroVelocity();
    this.p.body.data.type = 4;
    this.p.animations.play('underwater');
    this.p.body.clearShapes();
  }

  handleInput() {
    if (!this.p.target || this.p.target.alive === false) {
      this.p.target = null;
      this.switchTo(FloatIdleState);
      return;
    }
    if (Phaser.Point.distance(this.p.target, this.p) < TILE_SIZE * 1.2) {
      this.switchTo(JumpState);
    }
  }

  applyKick() {

  }
}

class JumpState extends MoveState {
  enter() {
    game.sounder.play('hello');
    this.p.inWater = false;
    this.p.shadow.visible = true;
    this.speed = 130;
    this.watchDogTimer = 2000;
    this.p.animations.play('jump');
    this.s = new Splash(this.p.x, this.p.y);
    this.p.body.clearShapes();
    this.p.body.addCircle(28, 0, -36);
    this.landing = false;
    game.add.tween(this.p.anchor)
      .to({ y: 1.3 }, 300, Phaser.Easing.Quadratic.Out)
      .start();
  }

  handleInput() {
    // AI
    if (!this.p.target || this.p.target.alive === false) {
      this.p.target = null;
      this.p.body.data.type = 1;
      this.switchTo(FlyingState);
      return;
    }
    if (!this.landing && Phaser.Point.distance(this.p.target, this.p) <= TILE_SIZE * 0.7) {
      this.landing = true;
      game.tweens.removeFrom(this.p);
      this.p.animations.play('land');
      game.add.tween(this.p.anchor)
        .to({ y: 1 }, 300, Phaser.Easing.Quadratic.In)
        .start()
        .onComplete.addOnce(() => this.switchTo(IdleState));
    }
  }

  update() {
    super.update();
    this.watchDogTimer -= game.time.physicsElapsedMS;
    if (this.watchDogTimer <= 0) {
      this.switchTo(IdleState);
    }
  }
}

class FlyingState extends PenguinState {
  enter() {
    this.p.inWater = false;
    this.p.trailEnabled = true;
    this.p.body.applyDamping = true;
    this.p.body.damping = 0.2;
    this.timer = 600;
    this.p.anchor.set(0.5, 1.3);
    this.p.isFlying = true;
    this.p.animations.play(game.rnd.pick(['stunned', 'tumbling']));
    if (this.p.target) {
      this.p.target.tint = this.p.target.baseTint || 0xFFFFFF;
    }
    this.p.target = null;
    if (this.p.body.velocity.x) {
      this.p.scale.setTo(-Math.sign(this.p.body.velocity.x) || 1, 1);
    }
  }

  handleInput() {

  }

  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer <= 0) {
      this.switchTo(LandOnZhopaState);
    }
  }
}

class PenguinTrail extends Phaser.Sprite {
  constructor(parent) {
    super(game, 0, 0, 'actors', 'penguin_i.png');
    this.anchor.set(0.5, 1);
    this.alpha = 0;
  }
}
