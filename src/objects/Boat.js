import Stateful, { DefaultState } from "./StatefulP2Sprite";
import { PROGRESS_WIN, REWARD_WOOD_BOAT } from "../consts";
import MagicText from "../effects/MagicText";

export default class Boat extends Stateful {
  constructor(x, y) {
    super(x, y, 'tiles', 'ship1.png');
    this.anchor.set(0.5, 1);
    this.body.clearShapes();
    this.body.addRectangle(400, 100, 0, -65);
    this.body.kinematic = true;

    this.setState(new DefaultState(this));
    this.progress = 0;

    this.label = game.add.text(0, -30, `0 / ${PROGRESS_WIN}`, { font: "bold 16px Arial" });
    this.label.anchor.set(0.5);
    this.addChild(this.label);
    let tween = game.add.tween(this.body).to({ y: this.body.y + 3 }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, -1);
    tween.yoyo(true, 0);
    this.waveTimer = 4000;
    this.addChild(new Overflow(0, 28));
    this.cargo = game.add.group();
    this.bort = game.add.sprite(-35, -56, 'tiles', 'ship_bort.png');
    this.addChild(this.cargo);
    this.cargo.addChild(this.bort);
    this.slots = [
      { x: -13, y: -124 },
      { x: 37, y: -124 },
      { x: 88, y: -124 },
      { x: -13, y: -94 },
      { x: 37, y: -94 },
      { x: 88, y: -94 },
      { x: -13, y: -156 },
      { x: 37, y: -156 },
      { x: 88, y: -156 },
      { x: -13, y: -123 },
      { x: 37, y: -123 },
      { x: 88, y: -123 },
    ]
    // fast debug
    // new MagicText(this.x, this.y, `+ ${REWARD_WOOD_BOAT}`, REWARD_WOOD_BOAT, 1);
  }

  addCargo(sp) {
    let slot = this.slots.shift();
    if (!slot) return;
    sp.x = slot.x;
    sp.y = slot.y;
    this.cargo.addChild(sp);
    this.bort.bringToTop();
  }

  update() {
    super.update();
    this.waveTimer -= game.time.physicsElapsedMS;
    if (this.waveTimer <= 0) {
      this.waveTimer = 4000;
      game.level.effectsGrp.add(new Wave(this.x, this.y - 60));
    }
  }

  onHit(body, bodyB, shapeA, shapeB, equation) {
    if (body.sprite.type && body.sprite.type === 'wood') {

      if (body.sprite.lastPlayerTouched) {
        body.sprite.lastPlayerTouched.score += REWARD_WOOD_BOAT;
        new MagicText(body.sprite.x, body.sprite.y - body.sprite.height, `+ ${REWARD_WOOD_BOAT}`, REWARD_WOOD_BOAT, body.sprite.lastPlayerTouched.id);
      }

      body.sprite.destroy();
      this.progress += 1;
      this.label.text = `${this.progress} / ${PROGRESS_WIN}`;
      this.addCargo(game.add.sprite(0, 0, 'tiles', 'box1_i.png'));
    }
  }

  get type() {
    return 'boat';
  }

  get sortY() {
    return this.y;
  }
}

class Wave extends Phaser.Sprite {
  constructor(x, y) {
    super(game, x, y, 'tiles', 'ship_waves.png');
    this.anchor.set(0.5, 0);
    game.add.tween(this.scale).to({ x: 1.1, y: 1.4 }, 800, Phaser.Easing.Quadratic.Out, true).onComplete.addOnce(() => this.destroy());
    game.add.tween(this).to({ alpha: 0 }, 800, Phaser.Easing.Quadratic.Out, true);
  }
}

class Overflow extends Phaser.Sprite {
  constructor(x, y) {
    super(game, x, y, 'tiles', 'ship_water.png');
    this.anchor.set(0.5, 1);
    let tween = game.add.tween(this).to({ y: this.y + 5 }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, -1);
    tween.yoyo(true, 0)
  }
}
