import Stateful, { DefaultState, Shadow, Splash } from "./StatefulP2Sprite";

export default class Wood extends Stateful {
  constructor(x, y, grid) {
    super(x, y, 'tiles', 'box1_i.png');
    this.grid = grid;
    this.anchor.set(0.5, 1);
    this.body.clearShapes();
    this.body.addRectangle(50, 50, 0, -25);
    // this.body.addCircle(25, 0, -32);
    this.body.damping = 0.9;
    this.body.mass = 20;
    this.shadow = new Shadow(0, 0, this, this.parent);
    this.shadowOffset = 8;
    this.cropRect = new Phaser.Rectangle(0, 0, this.width, this.height);
    this.originalHeight = this.height;
    this.setState(new IdleState(this));
  }

  appear() {
    this.setState(new AppearState(this));
  }

  onHit(body, bodyB, shapeA, shapeB, equation) {
    // console.log(body, bodyB, equation)
  }

  applyKick(force) {
    if (this.kickDisabled) return;
    this.body.data.type = 1;
    this.body.velocity.x = force.x;
    this.body.velocity.y = force.y;
    this.setState(new FlyingState(this));
  }

  get type() {
    return 'wood';
  }

  update() {
    super.update();
    //
  }

}

class IdleState extends DefaultState {
  enter() {
    this.p.inWater = false;
    this.p.cropRect.height = this.p.originalHeight;
    this.p.updateCrop();
    this.p.kickDisabled = false;
    game.tweens.removeFrom(this.p);
    game.tweens.removeFrom(this.p.cropRect);
    this.p.body.damping = 0.9;
    this.p.frameName = 'box1_i.png';
    this.p.shadow.visible = true;
    //console.log('enter idle state');
  }
  onHit(body) {
    if (body && body.sprite && body.sprite.isFlying) {
      this.p.body.data.type = 1;
      // const force = Phaser.Point.subtract(this.p, body.sprite).normalize().multiply(100, 100);
      // this.p.body.velocity.x = force.x;
      // this.p.body.velocity.y = force.y;
      this.switchTo(FlyingState);
    }

  }
  update() {
    //console.log(this.isMovedFromEnter(), this.gx, this.gy);
    if (!this.p.grid.isIce(this.gx, this.gy)) {
      this.switchTo(FloatState);
    }
  }
}

class FlyingState extends DefaultState {
  enter() {
    this.p.inWater = false;
    this.p.cropRect.height = this.p.originalHeight;
    this.p.updateCrop();
    game.tweens.removeFrom(this.p);
    game.tweens.removeFrom(this.p.cropRect);
    this.p.isFlying = true;
    this.p.body.damping = 0.3;
    this.timer = 800;
    this.p.anchor.setTo(0.5, 1.3);
    this.p.rameName = 'box1_i.png';
    this.p.shadow.visible = true;
  }
  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer <= 0) {
      this.switchTo(LandingState);
    }
  }
}


class FloatState extends DefaultState {
  enter() {
    this.p.inWater = true;
    this.p.kickDisabled = false;
    this.p.body.damping = 0.9;
    this.p.shadow.visible = false;
    this.s = new Splash(this.p.x, this.p.y);
    this.p.cropRect.height = this.p.originalHeight * 0.75;
    this.timer = 10000;
    // For some reason doesn't work correctly as expected with anchor.y = 1 (top of sprite doesn't move but should)
    let tween = game.add.tween(this.p.cropRect).to({ height: this.p.cropRect.height - 10 }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, -1);
    tween.yoyo(true, 0);
  }
  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer <= 0) {
      this.switchTo(DrowningState);
    }
    if (this.p.grid.isIce(this.gx, this.gy)) {
      this.switchTo(IdleState);
    }
    this.p.updateCrop();
  }
}

class DrowningState extends DefaultState {
  enter() {
    this.p.inWater = true;
    this.p.kickDisabled = true;
    this.p.body.destroy();
    this.p.anchor.setTo(0.5, 1);
    this.p.shadow.visible = false;
    game.tweens.removeFrom(this.p);
    game.tweens.removeFrom(this.p.cropRect);
    // For some reason doesn't work correctly as expected with anchor.y = 1 (top of sprite doesn't move but should)
    let t1 = game.add.tween(this.p.cropRect).to({ height: 28 }, 1000, Phaser.Easing.Quadratic.In, true);
    let t2 = game.add.tween(this.p).to({ alpha: 0 }, 300, Phaser.Easing.None);
    t1.chain(t2);
    t2.onComplete.addOnce(() => {
      this.p.destroy();
    });
    t1.start();
  }
  update() {
    this.p.updateCrop();
  }
}

class LandingState extends DefaultState {
  enter() {
    this.p.inWater = false;
    this.p.body.damping = 0.7;
    this.timer = 300;
    game.add.tween(this.p.anchor)
      .to({ y: 1 }, 300, Phaser.Easing.Bounce.Out)
      .start();
  }
  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer <= 0) {
      if (!this.p.grid.isIce(this.p.gx, this.p.gy)) {
        this.switchTo(FloatState);
      } else this.switchTo(IdleState);
    }
  }
}


class AppearState extends DefaultState {
  enter() {
    this.p.kickDisabled = true;
    this.p.anchor.setTo(0.5, 20);
    this.timer = 2000;
    // I want to sleep and I don't want more states
    this.p.shadow.alpha = 0;
    game.add.tween(this.p.shadow).to({ alpha: 1 }, 900, Phaser.Easing.None, true)
    game.add.tween(this.p.anchor)
      .to({ y: 1 }, 900, Phaser.Easing.None, true)
      .onComplete.addOnce(() => {
        if (!this.p.grid.isIce(this.p.gx, this.p.gy)) {
          this.switchTo(FloatState);
        } else {
          let t1 = game.add.tween(this.p.anchor)
            .to({ y: 3 }, 300, Phaser.Easing.Quadratic.Out);
          let t2 = game.add.tween(this.p.anchor)
            .to({ y: 1 }, 800, Phaser.Easing.Bounce.Out);
          t2.onComplete.addOnce(() => {
            this.switchTo(IdleState);
          });
          t1.chain(t2);
          t1.start();
        }
      });
  }
  onHit() {

  }
}
