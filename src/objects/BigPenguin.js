import Stateful, { DefaultState } from "./StatefulP2Sprite";

export default class BigPenguin extends Stateful {
  constructor(x, y) {
    super(x, y, 'bp', 'big_pen_f.png');
    this.anchor.set(0.5, 1);
    this.body.mass = 24;
    this.body.clearShapes();
    this.body.addCircle(80, 0, -50);
    this.body.damping = 0.9;

    this.setState(new SearchAndDestroy(this));
  }

  applyKick(f) {
    this.state.applyKick(f);
  }

  get type() {
    return 'boss';
  }
}

class Stunned extends DefaultState {
  enter() {
    this.p.body.setZeroVelocity();
    this.p.frameName = 'big_pen_s.png';
    this.stunTime = 2000;
  }

  applyKick(f) {
    this.stunTime = 2000;
  }

  update() {

    this.stunTime -= game.time.physicsElapsedMS;

    if (this.stunTime < 0) this.switchTo(SearchAndDestroy);
  }
}

class SearchAndDestroy extends DefaultState {
  enter() {
    this.p.frameName = 'big_pen_f.png';

    this.p.anchor.y = 0.8;

    game.add.tween(this.p.anchor).to({ y: 0.85 }, 600, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);


    this.speed = 20;
  }

  applyKick(f) {
    this.switchTo(Stunned);
  }

  handleInput() {
    this.p.target = game.level.grid.pickClosestTile(this.p.x, this.p.y);
  }


  update() {
    this.p.body.setZeroVelocity();

    if (!this.p.target) return;

    const target = Phaser.Point.subtract(this.p.target, this.p).normalize().multiply(this.speed, this.speed);
    this.p.scale.set(-Math.sign(target.x) || this.p.scale.x, 1);
    this.p.body.velocity.x = target.x;
    this.p.body.velocity.y = target.y;

    for (let ice of game.level.islandGrp.children.slice()) {
      if (ice.type && ice.type === 'ice' && ice.alive) {

        if (Phaser.Point.distance(this.p, ice) < 120) game.level.grid.removeTile(ice.gridX, ice.gridY);
      }
    }
  }
}
