import Stateful, { DefaultState, Shadow, Splash } from './StatefulP2Sprite';
import {PLAYER_SPEED_FLOAT, PLAYER_SPEED_ICE, PLAYER_KF_FLOAT, PLAYER_KF, Y_FACTOR, TILE_SIZE} from "../consts";
import MagicText from "../effects/MagicText";
import Combo from "../model/Combo";


export default class Player extends Stateful {
  // color = '2' or ''
  constructor(x, y, c = '') {
    super(x, y, 'actors', `player${c}_i.png`);

    this.id = c === '2' ? 2 : 1;

    this.animations.add('kick', [`player${c}_i.png`, `player${c}_k1.png`, `player${c}_k2.png`, `player${c}_k3.png`, `player${c}_k4.png`], 24);
    this.animations.add('kickStale', [`player${c}_k4.png`], 18);
    this.animations.add('idle', [`player${c}_i.png`], 18, true);
    this.animations.add('walk_side', Phaser.Animation.generateFrameNames(`player${c}_w`, 1, 12, '.png'), 18, true);
    this.animations.add('walk_up', Phaser.Animation.generateFrameNames(`player${c}_w`, 1, 12, '.png'), 18, true);
    this.animations.add('walk_down', Phaser.Animation.generateFrameNames(`player${c}_w`, 1, 12, '.png'), 18, true);
    this.animations.add('stunned', [`player${c}_s.png`], 18, true);
    this.animations.add('floating', [`player${c}_f1.png`, `player${c}_f2.png`, `player${c}_f3.png`, `player${c}_f4.png`, `player${c}_f5.png`], 18, true);
    this.anchor.set(0.5, 1);
    this.body.restitution = 0;
    this.body.clearShapes();
    // this.body.addRectangle(45, 30, 0, -10);
    this.body.addCircle(20, 0, -10);
    //this.body.updateCollisionMask();
    this.lookDirection = { x: 1, y: 0 };
    this.moveDirection = { x: 0, y: 0 };
    this.moveSpeed = 400;
    this.shadow = new Shadow(0, 0, this, this.parent);
    this.shadowOffset = 10;

    this.setState(new IdleState(this));

    this.combo = new Combo(this);
    this.score = 0;
  }

  get kickForce() {
    return this._inWater ? PLAYER_KF_FLOAT : PLAYER_KF;
  }

  attachCombo(combo) {
    // player does not chain combos
  }

  deattachCombo() {
    // does nothing
  }

  // for easier support of 2 gamepads etc
  assignController(controller) {
    this.controller = controller;
  }

  onHit(by) {
    this.state.onHit(by);
  }

  setState(state) {
    this.state = state;
  }



  kickInFront() {
    if (this.inFront.length) {
      game.sounder.play('hit');
    }
    for (let inFront of this.inFront) {
      inFront.attachCombo(this.combo);
      inFront && inFront.applyKick(Phaser.Point.subtract(inFront, {
        x: this.x,
        y: this.y + 15
      }).normalize().multiply(this.kickForce, this.kickForce));
    }

    if (this.combo.value > 1 && this.inFront.length) new MagicText(this.x, this.y - this.height, `x ${this.combo.value}`, this.combo.value, this.combo.player.id);
  }

  applyKick(force) {
    this.state.switchTo(StunnedState);
    this.body.velocity.x = force.x;
    this.body.velocity.y = force.y;
  }


  getIceCollider() {
    return new Phaser.Rectangle(this.x, this.y, 10, 5);
  }

  looksAt(obj) {
    let v1 = new Phaser.Point(this.lookDirection.x, this.lookDirection.y).normalize();
    let v2 = Phaser.Point.subtract(obj, this).normalize();
    let angle = v1.x * v2.x + v1.y * v2.y;
    return angle > 0;
  }

  update() {
    super.update();

    this.combo.update();

    this.inFront = [];
    let cP1Dist = 85;
    for (let c of game.level.actorsGrp.children) {
      if (c.type && c.type === 'boat') continue;

      if (c !== this && Phaser.Point.distance(c, this) < cP1Dist && this.looksAt(c)) {
        c.highlight({ x: this.x, y: this.y + 15 });
        this.inFront.push(c);
      }
      if (!c.alive) {
        c.destroy();
        game.level.penguinCounter += 1;
      }
    }
  }



  get type() {
    return 'player';
  }
}


class IdleState extends DefaultState {
  idleAnimation() {
    this.p.animations.play('idle');
  }

  enter() {
    this.p.inWater = false;
    // inertia?
    this.p.shadow.visible = true;
    this.p.isFlying = false;
    this.p.body.setZeroVelocity();
    this.idleAnimation();
  }

  handleInput() {
    let ctrl = this.p.controller;
    if (ctrl.pressedKick()) {
      this.kickWhileIdle();
    }
    if (ctrl.holdLeft() || ctrl.holdRight() || ctrl.holdUp() || ctrl.holdDown()) {
      if (this.p.standingOnIce()) {
        this.switchTo(MoveState);
      } else {
        this.switchTo(FloatMoving);
      }
    }
  }

  kickWhileIdle() {
    this.switchTo(KickState);
  }

  checkFloat() {
    // when someone break ice under me
    if (!this.p.standingOnIce()) {
      this.switchTo(FloatIdle);
      new Splash(this.p.x, this.p.y);
    }
  }

  update() {
    this.p.body.setZeroVelocity();

    // in case when it just stand on ice and ice got broken
    this.checkFloat();
  }
}

class KickState extends DefaultState {
  enter() {
    // TODO maybe leave some for inertia if not kicked anything
    this.p.body.setZeroVelocity();
    this.p.shadow.visible = true;
    this.timer = 500;
    this.p.animations.play('kick').onComplete.addOnce(() => {
      this.p.kickInFront();
      this.p.animations.play('kickStale');
    });
  }

  handleInput() {

  }

  update() {
    // on hit kick object back if never hit anything yet
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer <= 0) {
      this.switchTo(IdleState);
    }
  }
}

class StunnedState extends DefaultState {
  enter() {
    this.p.inWater = false;
    this.p.body.setZeroVelocity();
    this.timer = 400;
    this.p.animations.play('stunned');
    this.p.shadow.visible = true;
    this.p.isFlying = true;
  }

  handleInput() {

  }

  update() {
    this.timer -= game.time.physicsElapsedMS;
    if (this.timer <= 0) {
      if (this.p.standingOnIce()) {
        this.switchTo(IdleState);
      } else {
        // when i kicked into the water
        this.switchTo(FloatIdle);
        new Splash(this.p.x, this.p.y);
      }
    }
  }
}

class MoveState extends DefaultState {
  determineDirection(controller) {
    let d = { x: 0, y: 0 };
    if (controller.holdLeft()) {
      d.x = -1;
    } else if (controller.holdRight()) {
      d.x = 1;
    }
    if (controller.holdUp()) {
      d.y = -1;
    }
    if (controller.holdDown()) {
      d.y = 1;
    }
    return new Phaser.Point(d.x, d.y);
  }

  animateMovement(direction) {
    if (direction.y > 0) {
      this.p.animations.play('walk_down');
    } else if (direction.y < 0) {
      this.p.animations.play('walk_up');
    } else {
      this.p.animations.play('walk_side');
    }
  }

  enter() {
    this.p.inWater = false;
    this.p.moveDirection = this.determineDirection(this.p.controller);
    this.animateMovement(this.p.moveDirection);
    this.p.shadow.visible = true;
    this.p.moveSpeed = PLAYER_SPEED_ICE;
  }

  handleInput() {
    this.p.body.setZeroVelocity();

    // switch animation only if direction is different
    const oldDirection = this.p.moveDirection;
    this.p.moveDirection = this.determineDirection(this.p.controller);

    if (this.p.moveDirection.x !== oldDirection.x || this.p.moveDirection.y !== oldDirection.y) {
      this.animateMovement(this.p.moveDirection);
    }

    let speed = this.p.moveSpeed;
    if (this.p.moveDirection.x !== 0 && this.p.moveDirection.y !== 0) speed /= 1.4142; // sqrt(2)

    // actually move
    if (this.p.moveDirection.y === 1) {
      this.p.body.moveDown(speed);
    } else if (this.p.moveDirection.y === -1) {
      this.p.body.moveUp(speed);
    }
    if (this.p.moveDirection.x === 1) {
      this.p.body.moveRight(speed);
    } else if (this.p.moveDirection.x === -1) {
      this.p.body.moveLeft(speed);
    }

    if (this.p.moveDirection.x === 0 && this.p.moveDirection.y === 0) {
      this.stopMoving();
      return;
    } else {
      this.p.lookDirection = this.p.moveDirection;
      if (this.p.lookDirection.x) {
        this.p.scale.setTo(this.p.lookDirection.x || 1, 1);
      }
    }
    if (this.p.controller.pressedKick()) {
      this.kickWhileMove();
    }
  }

  kickWhileMove() {
    this.switchTo(KickState);
  }

  stopMoving() {
    this.switchTo(IdleState);
  }

  playMoveSound() {
    game.sounder.play('snow');
  }

  update() {
    super.update();
    this.playMoveSound();
    this.checkFloatMoving();
  }

  checkFloatMoving() {
    // when I hold move and ice breaks/ends
    if (!this.p.standingOnIce()) {
      this.switchTo(FloatMoving);
      new Splash(this.p.x, this.p.y);
    }
  }

}

class FloatIdle extends IdleState {
  enter() {
    super.enter();
    this.p.inWater = true;
    this.p.shadow.visible = false;
  }

  idleAnimation() {
    this.p.animations.play('floating');
  }

  checkFloat() {
    // do nothing.
  }

  kickWhileIdle() {
    this.switchTo(KickState);
  }
}

class FloatMoving extends MoveState {
  enter() {
    super.enter();
    this.p.inWater = true;
    this.p.shadow.visible = false;
    this.p.moveSpeed = PLAYER_SPEED_FLOAT;
  }

  playMoveSound() {
    // todo: swim sound?
  }

  checkFloatMoving() {
    // do nothing.
  }

  kickWhileMove() {
    this.switchTo(KickState);
  }

  stopMoving() {
    this.switchTo(FloatIdle);
  }

  update() {
    super.update();
    // when I hold move and ice on my way
    if (this.p.standingOnIce()) {
      this.switchTo(MoveState);
    }
  }

  animateMovement(direction) {
    this.p.animations.play('floating');
  }
}
