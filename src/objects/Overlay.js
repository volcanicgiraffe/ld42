let BitmapCache = new Map();

export default class Overlay extends Phaser.Sprite {
  constructor(colorStr = "black", alpha = 0.5, tintHex = 0xffffff) {
    super(game, 0, 0);
    //this.fixedToCamera = true;
    this.alpha = 0;
    this.targetAlpha = alpha;
    this.tint = tintHex;
    let bitmap = BitmapCache.get(colorStr) || ((function() {
      let bitmap = game.add.bitmapData(1, 1);
      bitmap.ctx.fillStyle = colorStr;
      bitmap.ctx.fillRect(0, 0, 1, 1);
      BitmapCache.set(colorStr, bitmap);
      return bitmap;
    }))();
    this.loadTexture(bitmap);
    this.width = game.canvas.width;
    this.height = game.canvas.height;
  }

  doShow() {
    this.width = game.canvas.width;
    this.height = game.canvas.height;
    return game.add.tween(this).to({alpha: this.targetAlpha}, 100, null, true);
  }
}
