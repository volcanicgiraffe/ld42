import { TILE_SIZE, Y_FACTOR } from "../consts";
import Arrow from "./Arrow";
import MagicText from "../effects/MagicText";

export default class StatefulWhatewer extends Phaser.Sprite {
  constructor(x, y, atlas, frame) {
    super(game, x, y, atlas, frame);
    game.physics.p2.enable(this, false);
    this.body.setZeroDamping();
    this.body.fixedRotation = true;
    this.body.collideWorldBounds = true;
    this.body.onBeginContact.add(this.onHit, this);

    this.highlightTimer = 0;
    this.comboDeattachTimer = 0;
    this._inWater = false;
  }

  highlight(source) {
    this.highlightSource = source;
    this.highlightTimer = 50;
  }

  applyKick() {
  }

  showArrow(source) {
    if (!this.arrow) {
      this.arrow = new Arrow();
      game.level.effectsGrp.add(this.arrow);
    }

    this.arrow.x = this.x;
    this.arrow.y = this.y - this.height / 2;
    this.arrow.visible = true;
    this.arrow.rotation = Phaser.Math.angleBetweenPoints(source, this);
  }

  hideArrow() {
    if (this.arrow) this.arrow.visible = false;
  }

  standingOnIce() {
    return game.level.grid.isIce(this.gx, this.gy)
  }

  get gx() {
    return Math.round((this.x - game.level.OFFSET_X) / TILE_SIZE);
  }

  get gy() {
    return Math.ceil((this.y + (this._inWater ? (game.level.waterLevel * -40) : 0) - game.level.OFFSET_Y) / TILE_SIZE / Y_FACTOR);
    // badly looks in below
    //return Math.round((this.y + (this._inWater ? (game.level.waterLevel * -40) : 20) - game.level.OFFSET_Y) / TILE_SIZE / Y_FACTOR);
  }

  get sortY() {
    return this.y + (this.standingOnIce() ? 1000 : 0);
  }

  onHit(body, bodyB, shapeA, shapeB, equation) {
    this.state.onHit(body, bodyB, shapeA, shapeB, equation);
  }

  attachCombo(combo) {
    combo.increment(1);
    this.comboDeattachTimer = 2000;

    this.lastPlayerTouched = combo.player;

    if (!this.combo) this.combo = combo;
  }

  deattachCombo() {
    this.combo = null;
  }

  update() {
    if (game.level.completed) return;

    if (this.comboDeattachTimer > 0) {
      this.comboDeattachTimer -= game.time.physicsElapsedMS;
      if (this.comboDeattachTimer <= 0) this.deattachCombo()
    }

    this.state.handleInput();
    this.state.update();

    this.highlightTimer -= game.time.physicsElapsedMS;

    if (this.highlightTimer > 0) {
      this.tint = 0x99FF99;
      this.showArrow(this.highlightSource);
    } else {
      this.tint = this.baseTint || 0xFFFFFF;
      this.hideArrow();
    }
  }

  set inWater(v) {
    if (this._inWater !== v) {
      this._inWater = v;
      v ? this.moveToWater() : this.moveToIce();
    }
  }

  moveToIce() {
    if (this.body) {
      this.body.y -= (game.level.waterLevel || 0) * 42;
    }
  }

  moveToWater() {
    if (this.body) {
      this.body.y += (game.level.waterLevel || 0) * 42
    };
  }

  setState(state) {
    this.state = state;
  }

  destroy() {
    if (this.arrow) this.arrow.destroy();
    if (this.shadow) this.shadow.destroy();
    super.destroy();
  }
}

export class DefaultState {
  constructor(parent) {
    this.p = parent;
    this.enterXY = {x: parent.x, y: parent.y, gx: parent.gx, gy: parent.gy};
    this.enter();
  }

  get gx() {
    return this.isMovedFromEnter() ? this.p.gx : this.enterXY.gx;
  }

  get gy() {
    return this.isMovedFromEnter() ? this.p.gy : this.enterXY.gy;
  }

  isMovedFromEnter(distance = 4) {
    return Math.hypot(this.p.x - this.enterXY.x, this.p.y - this.enterXY.y) > distance;
  }


  enter() {
  }

  handleInput() {
  }

  applyKick(direction, force) {
  }

  update() {
  }

  onHit() {
  }

  switchTo(state) {
    this.p.setState(new state(this.p));
  }
}

export class Shadow extends Phaser.Sprite {
  constructor(x, y, owner, grp) {
    super(game, x, y, 'actors', 'shadow.png');
    this.anchor.set(0.5, 1);
    this._owner = owner;
    this.alpha = 0.6;
    game.level.shadowsGrp.add(this);
  }

  update() {
    this.x = this._owner.x;
    this.y = this._owner.y + (this._owner.shadowOffset || 50);
  }

  get sortY() {
    return this._owner.sortY - 0.5;
  }
}

export class Splash extends Phaser.Sprite {
  constructor(x, y) {
    super(game, x, y, 'actors', 'splash1.png');
    this.animations.add('bulk', Phaser.Animation.generateFrameNames('splash', 1, 10, '.png'), 18)
      .play()
      .onComplete.addOnce(() => {
        this.destroy()
      });
    game.sounder.play('splash');
    this.anchor.set(0.5, 1);
    game.level.effectsGrp.add(this);
  }
}
